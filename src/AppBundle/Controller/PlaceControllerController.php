<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Place;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;


class PlaceControllerController extends Controller
{
    /**
     * @Route("/places",name="places_list")
     * @Method({"GET"})
     */
    public function getPlcacesAction()
    {
        $places = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Place')->findAll();
        /* @var $places Place[]*/
        $formatted = [];
        foreach ($places as $place){
            $formatted[] = [
               'id' => $place->getId(),
                'name' => $place->getName(),
                'address' => $place->getAddress(),
            ];
        }

        return new JsonResponse($formatted);
    }

}
